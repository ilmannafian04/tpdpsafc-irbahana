import React, { useState } from 'react';
import Revealed from './Revealed';

function App() {
  const [isRevealed, setIsRevealed] = useState(false);
  const audios = [new Audio(`${process.env.PUBLIC_URL}/confetti.mp3`), new Audio(`${process.env.PUBLIC_URL}/song.mp3`)];
  return !isRevealed
    ? (
      <div>
        <img src={`${process.env.PUBLIC_URL}/red-button.jpg`} onClick={() => setIsRevealed(true)} alt="button"></img>
        <br/>
        <span>audio warning on next page</span>
      </div>
    )
    : <Revealed audios={audios} />;
}

export default App;
