import React, { FunctionComponent, useState, useEffect } from 'react';
import Confetti from 'react-confetti';

interface RevealedProps {
    audios: HTMLAudioElement[]
}

const Revealed: FunctionComponent<RevealedProps> = ({audios}) => {
    const [hasPlayed, setHasPlayed] = useState(false);
    useEffect(() => {
        if (!hasPlayed && audios) {
            audios.forEach((audio) => {
                audio.play();
                audio.volume = 0.2;
            });
            setHasPlayed(true);
        }
    }, [hasPlayed, audios]);
    return (
        <div>
            <h1>HAPPY BIRTHDAY PAL</h1>
            <span>Scroll to right if using mobile</span>
            <div style={{display: "flex", alignItems: "flex-end"}}>
                <img src={`${process.env.PUBLIC_URL}/ilman.jpg`} width="375" height="500" alt="ilman"></img>
                <img src={`${process.env.PUBLIC_URL}/clap.gif`} alt="clap"></img>
                <img src={`${process.env.PUBLIC_URL}/pal.png`} width="375" height="500" alt="pal"></img>
            </div>
            <button onClick={() => audios.forEach((audio) => audio.pause())}>stop audio</button>
            <br></br>
            <span>A gift: MDGAL-ERZ80-(5 digit code that i dmed you last night)<br/>issa steam giftcard</span>
            <Confetti width={1920} />
        </div>
    );
};

export default Revealed;